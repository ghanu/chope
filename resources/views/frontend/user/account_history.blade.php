@extends('frontend.layouts.app')

@section('content')
    <div class="row justify-content-center align-items-center mb-3">
        <div class="col col-sm-10 align-self-center">
            <div class="card">
                <div class="card-header">
                    <strong>
                        {{ __('navs.frontend.user.account_history') }}
                    </strong>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <h4 class="card-title mb-0">
                                {{ __('Description') }}
                                <small class="text-muted">{{ __('Test') }}</small>
                            </h4>
                        </div><!--col-->
                    </div><!--row-->

                    <div class="row mt-4 mb-4">
                        <div class="col">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-expanded="true"><i class="fa fa-user"></i> {{ __('labels.backend.access.users.tabs.titles.overview') }}</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="overview" role="tabpanel" aria-expanded="true">
                                    <div class="row mt-4">
                                        <div class="col">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>{{ __('Description') }}</th>
                                                        <th>{{ __('Created At') }}</th>
                                                        <th>{{ __('Updated At') }}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if (count($userHistory))
                                                        @foreach ($userHistory as $user)
                                                            <tr>
                                                                <td>{{ $user['description'] }}</td>
                                                                <td>{{ $user['created_at'] }}</td>
                                                                <td>{{ $user['updated_at'] }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr><td colspan="9"><p class="text-center">{{ __('strings.backend.access.users.no_deactivated') }}</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!--col-->
                                    </div><!--row-->
                                </div><!--tab-->
                            </div><!--tab-content-->
                        </div><!--col-->
                    </div><!--row-->
                </div><!--card-body-->
            </div><!-- card -->
        </div><!-- col-xs-12 -->
    </div><!-- row -->
@endsection
