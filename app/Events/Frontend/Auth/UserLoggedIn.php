<?php

namespace App\Events\Frontend\Auth;

use App\Models\Auth\User;
use Illuminate\Queue\SerializesModels;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Facades\Cache;

/**
 * Class UserLoggedIn.
 */
class UserLoggedIn
{
    use SerializesModels;

    /**
     * @var
     */
    public $user;

    /**
     * @param $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        Activity()->log('User Login Successfully!!!');

        $history = Activity::where('causer_id', $user->id)->get();

        //Put cache
        Cache::put('profile_history_' . $user->id, $history->toArray());

    }
}
