<?php

namespace App\Http\Controllers\Api\Auth;


use Spatie\Activitylog\Models\Activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

/**
 * Class AccountController.
 */
class AccountController extends BaseController
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function history(Request $request)
    {
        $userId =  $request->only('id');
        if (Cache::has('profile_history' . $userId)) {
            return Cache::get('profile_history_' . $userId);
        }
        $userHistory = Activity::where('causer_id', $userId);
        return $this->sendResponse($userHistory);
    }
}
