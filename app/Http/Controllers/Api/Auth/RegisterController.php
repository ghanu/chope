<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Foundation\Auth\RegistersUsers;
use App\Repositories\Frontend\Auth\UserRepository;
use Illuminate\Http\Request;

/**
 * Class RegisterController.
 */
class RegisterController extends BaseController
{
    use RegistersUsers;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * RegisterController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Throwable
     */
    public function register(Request $request)
    {
        try {
            $user = $this->userRepository->create($request->only('first_name', 'last_name', 'email', 'password'));
            return $this->sendResponse($user->toArray(), 'Successfully Register');
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), $e->getMessage());
        }
    }
}
