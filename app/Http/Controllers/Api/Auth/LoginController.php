<?php

namespace App\Http\Controllers\Api\Auth;

use App\Helpers\Auth\Auth;
use App\Http\Controllers\Api\Auth\BaseController;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Helpers\Frontend\Auth\Socialite;
use App\Events\Frontend\Auth\UserLoggedIn;
use App\Events\Frontend\Auth\UserLoggedOut;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Repositories\Frontend\Auth\UserSessionRepository;

/**
 * Class LoginController.
 */
class LoginController extends BaseController
{
    use AuthenticatesUsers;


    public function login(Request $request)
    {
        $isLogIn = $this->guard()->attempt($this->credentials($request), $request->filled('remember'));
        if ($isLogIn) {
            $response = $this->authenticated($request, $this->guard()->user());
            return $response;
        }
        return $this->sendError('User Name or Password wrong');
    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param         $user
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    protected function authenticated(Request $request, $user)
    {
        /*
         * Check to see if the users account is confirmed and active
         */
        if (! $user->isConfirmed()) {

            // If the user is pending (account approval is on)
            if ($user->isPending()) {
                $this->sendError(['Not activated'], ['Email approval is pending']);
            }

            // Otherwise see if they want to resent the confirmation e-mail
            throw new GeneralException(__('exceptions.frontend.auth.confirmation.resend', ['user_uuid' => $user->{$user->getUuidName()}]));
        } elseif (! $user->isActive()) {
            $this->sendError(['Not activated'], ['Email not activated yet']);
        }

        return $this->sendResponse($user, 'Login Success fully');
    }
}
