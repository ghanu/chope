<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Facades\Cache;

/**
 * Class AccountController.
 */
class AccountController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.user.account');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function history()
    {
        $userId = auth()->user()->id;

        if (Cache::has('profile_history_' . $userId)) {
            var_dump('s');
            return Cache::get('profile_history_' . $userId);
        }

        $userHistory = Activity::where('causer_id', $userId)->get();
        return view('frontend.user.account_history', ['userHistory' => $userHistory->toArray()]);
    }
}
