<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    //return $request->user();
});


/**
 * API Access Controllers
 * All route names are prefixed with 'api.auth'.
 */

Route::group(['namespace' => 'Api\Auth', 'as' => 'api'], function () {


    /**
     * These routes require the user to be logged in
     */
    Route::group(['middleware' => 'auth'], function () {
        Route::post('history', 'AccountController@history')->name('history.post');
        Route::get('history', 'AccountController@history')->name('history.post');
    });

    /*
     * These routes require no user to be logged in
     */
    Route::group(['middleware' => 'guest'], function () {
        // Authentication Routes
        Route::post('login', 'LoginController@login')->name('login.post');
        Route::get('login', 'LoginController@login')->name('login.post');

        // Socialite Routes
        Route::get('login/{provider}', 'SocialLoginController@login')->name('social.login');
        Route::get('login/{provider}/callback', 'SocialLoginController@login');

        // Registration Routes
        Route::post('register', 'RegisterController@register')->name('register.post');
        Route::get('register', 'RegisterController@register')->name('register.post');

        // Confirm Account Routes
        //Route::get('account/confirm/{token}', 'ConfirmAccountController@confirm')->name('account.confirm');
        //Route::get('account/confirm/resend/{uuid}', 'ConfirmAccountController@sendConfirmationEmail')->name('account.confirm.resend');

        // Password Reset Routes
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.email');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email.post');

        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset.form');
        Route::post('password/reset', 'ResetPasswordController@reset')->name('password.reset');
    });
});