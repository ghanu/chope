## Chope Assignment
### Register feature
- The user can register on your web application by filling in, at least, the username and password. You can decide what information is worth to collect from your perspective and from the business point of view.
- User's behaviors like register should be stored in Redis properly.
### Login feature
 1. Existing users can login successfully when they have valid username and password. On the contrary, the user should be indicated with a proper message when fails to login.
 2. Logged in users can logout and this action should be recorded in Redis, too.
 3. Logged in users can see their register/login/logout history retrieved from Redis.
### API (Restful)
1. Register API
2. Allow mobile apps to implement register feature by building-in this API
3. Login API
  i. Allow mobile apps to implement login feature by building-in this API
  
##Install docker
###To install docker and docker compose to your instance

##Run docker 
```docker-compose up -d nginx mysql phpmyadmin redis workspace```

##Run deploy.sh
```
docker exec -it chope-dock_workspace_1 bash

sh deploy.sh

```

##Run phpunit
```
docker exec -it chope-dock_workspace_1 bash 

phpunit

```

##Access Site
```http://chope.mles.co.in/```

##Api End Points
```http://chope.mles.co.in/api/login```
```http://chope.mles.co.in/api/register```
```http://chope.mles.co.in/api/history```

##Login Api
```
    /**
        * Login the user
        *
        * Uses the site's api_key to login the user and fetch the user's api keys
        *
        * ### Route
        *
        * > **POST** /api/login
        *
        * ### Request Parameters
        *
        * | Parameters     | Required? | Description
        * | :------------- | :-------- | :----------
        * | `email`        | required  |
        * | `password`     | required  |
        * | `platform`     | optional  | Either "iOS" or "android" or "androidcn" for china android
        * | `device_token` | optional  | Device token as provided by device
        *
        * ### Example Response
        * 
        * {
          	"success": true,
          	"data": {
          		"id": 18,
          		"uuid": "7d03b94d-8959-4986-be3c-cb2cc5e81e57",
          		"first_name": "kkk",
          		"last_name": "kkkkk",
          		"email": "ghanuss@gmail.com",
          		"avatar_type": "gravatar",
          		"avatar_location": null,
          		"password_changed_at": null,
          		"active": 1,
          		"confirmation_code": "6b564827449307cb10ac11cd9d2a6fd7",
          		"confirmed": 1,
          		"timezone": "UTC",
          		"created_at": "2018-04-15 15:56:13",
          		"updated_at": "2018-04-15 15:56:13",
          		"deleted_at": null,
          		"full_name": "kkk kkkkk"
          	},
          	"message": "Login Success fully"
          }
     */   
```

##Register Api
``` 
    /**
        * Login the user
        *
        * Uses the site's api_key to login the user and fetch the user's api keys
        *
        * ### Route
        *
        * > **POST** /api/login
        *
        * ### Request Parameters
        *
        * | Parameters     | Required? | Description
        * | :------------- | :-------- | :----------
        * | `email`        | required  |
        * | `password`     | required  |
        * | `first_name`   | optional  | Either "iOS" or "android" or "androidcn" for china android
        * | `last_name` | optional  | Device token as provided by device
        *
        * ### Example Response
        * 
        * {
          	"success": true,
          	"data": {
          		"first_name": "kkk",
          		"last_name": "k333",
          		"email": "ghanusss@gmail.com",
          		"confirmation_code": "6998406666f74f74dbbf2b0a34b63470",
          		"active": 1,
          		"confirmed": 1,
          		"uuid": "40638d8e-60a1-40b8-ac7b-96a844b21b26",
          		"updated_at": "2018-04-15 16:26:44",
          		"created_at": "2018-04-15 16:26:44",
          		"id": 19,
          		"full_name": "kkk k333"
          	},
          	"message": "Successfully Register"
          }
    */    
```

##Login as admin
```
username : admin@admin.com
password : 1234
```
### To Do
1. Store user information into redis
2. retrieved user and history information from redis.  
